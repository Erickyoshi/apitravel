export class Client {
    public id_client: number
    public name: string
    public phone: number
    public address: string
    public email: string
    public id_travel: number

}

export class RegisterClient {
    public name: string
    public phone: number
    public address: string
    public email: string

    constructor(name: string,
                phone: number,
                address: string,
                email: string) {
        
        this.name = name
        this.phone = phone
        this.address = address
        this.email = email
    }
}