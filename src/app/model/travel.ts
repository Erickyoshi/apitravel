export class Travel {
    public id_travel: number
    public country: string
    public price: number
    public days: number
    public name: string
    public amount_people: number
    public description: string
    public payment_method: string  
}
