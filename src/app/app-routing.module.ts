import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginClient } from './ui/login-client/login-client.component';
import { TravelPage } from './ui/travel-page/travel-page.component';
import { RegisterClient } from './ui/register-client/register-client.component';

const routes: Routes = [
  { path: 'login', component: LoginClient},
  { path: 'travel', component: TravelPage},
  { path: 'register', component: RegisterClient}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
