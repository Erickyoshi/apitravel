import { Component, OnInit} from '@angular/core'
import {Validators, FormBuilder} from "@angular/Forms";
import { Router } from '@angular/router';

@Component({
    selector: 'app-register-client',
    templateUrl: './register-client.component.html',
    styleUrls: ['./register-client.component.css']
})

export class RegisterClient implements OnInit{

    registerValidatorForm = this.formBuilder.group({
        username: ['', [Validators.required]],
        password: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]]
    })
    constructor(private formBuilder: FormBuilder,
                private router: Router){}

    ngOnInit(): void {

    }

    goToLogin() {
        this.router.navigateByUrl("/login")
    }
}