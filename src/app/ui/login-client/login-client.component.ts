
import { Component, OnInit} from '@angular/core'
import {Validators, FormBuilder} from "@angular/Forms";
import { Router } from '@angular/router';

@Component({
    selector: 'app-login-client',
    templateUrl: './login-client.component.html',
    styleUrls: ['./login-client.component.css']
})

export class LoginClient implements OnInit{
   
    loginValidatorForm = this.formBuilder.group({
        username: ['', [Validators.required]],
        password: ['', [Validators.required]]
    })

    constructor(private formBuilder: FormBuilder,
        private router: Router){}

    ngOnInit(): void {
        
    }

    goToTravel() {
        this.router.navigateByUrl("/travel")
    }
}